= Normalització
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:

<<<

== Modelat ER

=== Introducció

Una teoria important que s'ha desenvolupat pel model entitat relació (ER)
implica la noció de dependència funcional (FD). L'objectiu d'estudiar això
és millorar la comprensió de les relacions entre les dades i guanyar prou
formalisme per tal ens assisteixi en el disseny pràctic de bases de dades.

Com les restriccions, les FD s'extreuen de la semàntica del domini de
l'aplicació. Essencialment, les _dependències funcionals_ descriuen estan
relacionats els atributs individuals. Les FD són un tipus de restricció entre
atributs dins d'una relació i contribueixen al disseny d'un bon esquema
relacional. En aquesta secció parlarem de:

* La teoria bàsica i definició de dependència funcional.
* La metodologia per millorar el disseny d'equema, també anomenada
normalització.

=== Disseny relacional i redundància

Generalment, un bon disseny de bases de dades relacional ha de capturar tots
els atributs i associacions necessàries. El disseny hauria de fer això amb
una quantitat mínim d'informació emmagatzemada i sense dades redundants.

En disseny de bases de dades, la redundància és normalment indesitjable perquè
causa problemes a l'hora de mantenir la consistència durant les actualitzacions.
Tanmateix, la redundància de vegades pot implicar millores en el rendiment; per
exemple, quan la redundància es pot utilitzar en lloc d'un _join_ per connectar
dades. Un _join_ s'utilitza quan necessitem obtenir informació basada en dues
taules relacionades.

Considerem la següent figura: el client 1313131 es mostra dues vegades, una
pel compte número A-101 i un altre cop pel compte A-102. En aquest cas, el
número de client no és redundant, tot i que hi ha anomalies d'esborrat a la
taula. Aquest problema es pot solucionar si tenim una taula customer
separada. Tanmateix, si una adreça de sucursal (_branch_) ha de canviar, s'haurà
d'actualitzar el múltiples llocs. Si el nombre de client es deixés a la taula
tal com és, aleshores no necessitaríem una taula per les sucursals i no es
necessitaria cap join, cosa que milloraria el rendiment.

.Un exemple de redundància utilitzant amb comptes bancaria i branques.
image::images/bank_accounts.jpg[Un exemple de redundància utilitzant amb comptes bancaria i branques]

=== Anomalia d'inserció

Una _anomalia d'inserció_ succeeix quan s'està insertant informació inconsistent
en una taula. Quan inserim un nou registre, com un compte de número A-306 a la
figura següent, hem de comprovar que les dades de la sucursal són consistents
amb les files existents.

.Exemple d'anomalia d'inserció.
image::images/insertion_anomaly_bank_accounts.jpg[Exemple d'anomalia d'inserció]

=== Anomalia d'actualització

Si una sucursal canvia d'adreça, com ara la sucursal de Round Hill de la figura
següent, hem d'actualitzar totes les files que es refereixen a aquesta
sucursal. El canvi d'informació existent de forma incorrecta s'anomena una
_anomalia d'actualització_.

.Exemple d'anomalia d'actualització.
image::images/update_anomaly_bank_accounts.jpg[Exemple d'anomalia d'actualització]

=== Anomalia d'eliminació

Una _anomalia d'eliminació_ ocorre quan s'esborra un registre que pot contenir
atributs que no s'haurien d'esborrar. Per exemple, si eliminèssim informació
sobre l'últim compte d'una sucursal, com el compte A-101 de la sucursal de
Downtown de la figura següent, tota la informació de la sucursal
desapareixeria.

.Exemple d'anomalia d'eliminació.
image::images/deletion_anomaly_bank_accounts.jpg[Exemple d'anomalia d'eliminació]

El problema que hi ha si esborrem la fila A-101 és que no sabrem on està
localitzada la sucursal de Downtown i perdrem tota la informació corresponent
al client 1313131. Per evitar aquests tipus de problemes d'actualització o
eliminació, necessitem descomposar la taul original en diverses taules més
petites de manera que cada taula tingui el mínim solapament possible amb les
altres taules.

Cada taula del sistema de comptes bancaris ha de contenir informació només
sobre una entitat, com les sucursals o els clients, com es mostra a la figura:

.Exemples de taula de comptes bancaris que contenen una entitat cadascuna.
image::images/branch_to_customer_ERD.jpg[Exemples de taula de comptes bancaris que contenen una entitat cadascuna]

Si seguim aquesta pràctica ens assegurarem que quan s'afegeix o actualitza
la informació d'una sucursal només es veurà afectat un registre. Així, quan
la informació dels clients s'afegeix o s'esborra, la informació de la sucursal
no es veurà modificada accidentalment o incorrectament guardada.

==== Exemple: taules i anomalies per projectes i empleats

La següent figura mostra un exemple d'una taula per projectes i empleats.
D'aquesta taula podem assumir que:

1. L'EmpID i el ProjectID són una clau primària composta.
2. L'ID del projecte determina el pressupost (budget). Per exemple, el projecte
P1 té un pressupost de 32 hores.

.Exemple d'una taula per projectes i empleats.
image::images/project_emp_table.jpg[Exemple d'una taula per projectes i empleats]

A continuació anem a veure algunes de les possibles anomalies que poden ocórrer
amb aquesta taula durant els següents passos:

1. Acció: afegir la fila {S85, 35, P1, 9}.
2. Problema: hi ha dues tuples amb pressupostos conflictius.
3. Acció: esborrar la tupla {S79, 27, P3, 1}.
4. Problema: el pas #3 esborra el pressupost pel projecte P3.
5. Acció: actualitzar la tupla {S75, 32, P1, 7} a {S75, 35, P1, 7}.
6. Problema: el pas #5 crear dues tuples amb valors diferents pel pressupost
del projecte P1.
7. Solució: crear taules separades, una pels projectes i l'altra pels empleats,
com es mostra a la següent figura:

.Solució: separació de taules per projectes i empleats.
image::images/project_to_emp_ERD.jpg[Solució: separació de taules per projectes i empleats]

=== Com evitar les anomalies

La millor aproximació per crear taules sense anomalies és assegurar-se que les
taules estan normalitzades, i això s'aconsegueix entenent les dependències
funcionals. Les FD asseguren que tots els atributs en una taula pertanyen a
aquella taula. En altres paraules, s'eliminaran redundàncies i anomalies.

==== Exemple: separació de les taules Project i Employee

.Separació de les taules Project i Employee, amb dades.
image::images/project_and_emp_tables.jpg[Separació de les taules Project i Employee, amb dades]

Bo i mantenint les dades separades utilitzant taules diferents per projectes
i empleats:

1. No es crearan anomalien si es canvia un pressupost.
2. No es necessiten valors comodí pels projectes que no tinguin cap empleat
assignat.
3. Si s'esborra la contribució d'un empleat, no es perden dades importants.
4. No es crearan anomalies si s'afegeix la contribució d'un empleat.

=== Termes claus

*anomalia d'eliminació*: succeeix quan esborrem un registre que pot contenir
atributs que no s'haurien d'esborrar.

*dependència funcional (FD)*: descriu com es relacionen els atributs
individuals.

*anomalia d'inserció*: succeeix quan s'insereix informació inconsistent en
una taula.

*join*: s'utilitza quan necessitem obtenir informació basada en dues taules
relacionades.

*anomalia d'actualització*: succeeix quan modifiquem informació existent de
forma incorrecta.

== Dependències funcionals

=== Introducció

A _functional dependency_ (FD) is a relationship between two attributes,
typically between the PK and other non-key attributes within a table.
For any relation R, attribute Y is functionally dependent on attribute X
(usually the PK), if for every valid instance of X, that value of X
uniquely determines the value of Y. This relationship is indicated by
the representation below :

*X ———–> Y*

The left side of the above FD diagram is called the _determinant_, and
the right side is the _dependent_. Here are a few examples.

In the first example, below, SIN determines Name, Address and Birthdate.
Given SIN, we can determine any of the other attributes within the
table.

*SIN   ———-> Name, Address, Birthdate*

For the second example, SIN and Course determine the date completed
(DateCompleted). This must also work for a composite PK.

*SIN, Course  ———>     DateCompleted*

The third example indicates that ISBN determines Title.

*ISBN  ———–>  Title*

=== Regles de dependències funcionals

Consider the following table of data r(R) of the relation schema
R(ABCDE) shown in Table 11.1.

http://opentextbc.ca/dbdesign01/wp-content/uploads/sites/11/2013/12/Table-R-Functional-Dependency-example.jpg[image:Database%20Design%20-%202nd%20Edition_fitxers/Table-R-Functional-Dependency-example.jpg[Table-R-Functional-Dependency-example]]

Table 11.1. Functional dependency example, by A. Watt.

As you look at this table, ask yourself: _What kind of dependencies can
we observe among the attributes in Table R?_ Since the values of A are
unique (a1, a2, a3, etc.), it follows from the FD definition that:

A → B,    A → C,    A → D,    A → E

* It also follows that  A →BC  (or any other subset of ABCDE).
* This can be summarized as   A →BCDE.
* From our understanding of primary keys, A is a primary key.

Since the values of E are always the same (all e1), it follows that:

A → E,   B → E,   C → E,   D → E

However, we cannot generally summarize the above with  ABCD → E 
because, in general,   A → E,   B → E,   AB → E.

Other observations:

1.  Combinations of BC are unique, therefore  BC → ADE.
2.  Combinations of BD are unique, therefore  BD → ACE.
3.  If C values match, so do D values.
1.  Therefore,  C → D
2.  However, D values don’t determine C values
3.  So C does not determine D, and D does not determine C.

Looking at actual data can help clarify which attributes are dependent
and which are determinants.

=== Regles d'inferència

_Armstrong’s axioms_ are a set of inference rules used to infer all the
functional dependencies on a relational database. They were developed by
William W. Armstrong. The following describes what will be used, in
terms of notation, to explain these axioms.

Let R(U) be a relation scheme over the set of attributes U. We will use
the letters X, Y, Z to represent any subset of and, for short, the union
of two sets of attributes, instead of the usual  X U Y.

Axiom of reflexivity
^^^^^^^^^^^^^^^^^^^^

This axiom says, if Y is a subset of X, then X determines Y (see Figure
11.1).

http://opentextbc.ca/dbdesign01/wp-content/uploads/sites/11/2013/12/Ch-11-Axion-Reflexivity.jpg[image:Database%20Design%20-%202nd%20Edition_fitxers/Ch-11-Axion-Reflexivity.jpg[Ch-11-Axion-Reflexivity]]

Figure 11.1. Equation for axiom of reflexivity.

For example, *PartNo —> NT123*  where X (PartNo) is composed of more
than one piece of information; i.e., Y (NT) and partID (123).

Axiom of augmentation
^^^^^^^^^^^^^^^^^^^^^

The axiom of augmentation, also known as a partial dependency, says if X
determines Y, then XZ determines YZ for any Z (see Figure 11.2 ).

http://opentextbc.ca/dbdesign01/wp-content/uploads/sites/11/2013/12/Ch-11-Axiom-of-Augmentation-300x34.jpg[image:Database%20Design%20-%202nd%20Edition_fitxers/Ch-11-Axiom-of-Augmentation-300x34.jpg[Ch-11-Axiom-of-Augmentation-300x34]]

Figure 11.2. Equation for axiom of augmentation.

The axiom of augmentation says that every non-key attribute must be
fully dependent on the PK. In the example shown below, StudentName,
Address, City, Prov, and PC (postal code) are only dependent on the
StudentNo, not on the StudentNo and Grade.

StudentNo, Course —> StudentName, Address, City, Prov, PC, Grade,
DateCompleted

This situation is not desirable because every non-key attribute has to
be fully dependent on the PK. In this situation, student information is
only partially dependent on the PK (StudentNo).

To fix this problem, we need to break the original table down into two
as follows:

* Table 1: StudentNo, Course,  Grade, DateCompleted
* Table 2: StudentNo, StudentName, Address, City, Prov, PC

Axiom of transitivity
^^^^^^^^^^^^^^^^^^^^^

The axiom of transitivity says if X determines Y, and Y determines Z,
then X must also determine Z (see Figure 11.3).

http://opentextbc.ca/dbdesign01/wp-content/uploads/sites/11/2013/12/Ch-11-Axiom-of-transitivity-300x30.jpg[image:Database%20Design%20-%202nd%20Edition_fitxers/Ch-11-Axiom-of-transitivity-300x30.jpg[Ch-11-Axiom-of-transitivity-300x30]]

Figure 11.3. Equation for axiom of transitivity.

The table below has information not directly related to the student; for
instance, ProgramID and ProgramName should have a table of its own.
ProgramName is not dependent on StudentNo; it’s dependent on ProgramID.

StudentNo  —> StudentName, Address, City, Prov, PC, ProgramID,
ProgramName

This situation is not desirable because a non-key attribute
(ProgramName) depends on another non-key attribute (ProgramID).

To fix this problem, we need to break this table into two: one to hold
information about the student and the other to hold information about
the program.

* Table 1: StudentNo —> StudentName, Address, City, Prov, PC, ProgramID
* Table 2: ProgramID —> ProgramName* +
*

However we still need to leave an FK in the student table so that we can
identify which program the student is enrolled in.

Union
^^^^^

This rule suggests that if two tables are separate, and the PK is the
same, you may want to consider putting them together. It states that if
X determines Y and X determines Z then X must also determine Y and Z
(see Figure 11.4).

http://opentextbc.ca/dbdesign01/wp-content/uploads/sites/11/2013/12/Ch-11-Axiom-Union-300x23.jpg[image:Database%20Design%20-%202nd%20Edition_fitxers/Ch-11-Axiom-Union-300x23.jpg[Ch-11-Axiom-Union-300x23]]

Figure 11.4. Equation for the Union rule.

For example, if:

* SIN —> EmpName
* SIN —> SpouseName

You may want to join these two tables into one as follows:

SIN –> EmpName, SpouseName

Some database administrators (_DBA_) might choose to keep these tables
separated for a couple of reasons. One, each table describes a different
entity so the entities should be kept apart. Two, if SpouseName is to be
left NULL most of the time, there is no need to include it in the same
table as EmpName.

Decomposition
^^^^^^^^^^^^^

Decomposition is the reverse of the Union rule. If you have a table that
appears to contain two entities that are determined by the same PK,
consider breaking them up into two tables. This rule states that if X
determines Y and Z, then X determines Y and X determines Z separately
(see Figure 11.5).

http://opentextbc.ca/dbdesign01/wp-content/uploads/sites/11/2013/12/Ch-11-Axiom-Decomposition-300x28.jpg[image:Database%20Design%20-%202nd%20Edition_fitxers/Ch-11-Axiom-Decomposition-300x28.jpg[Ch-11-Axiom-Decomposition-300x28]]

Figure 11.5. Equation for decompensation rule.

=== Diagrama de dependències

A dependency diagram, shown in Figure 11.6, illustrates the various
dependencies that might exist in a _non-normalized table_. A
non-normalized table is one that has data redundancy in it.

http://opentextbc.ca/dbdesign01/wp-content/uploads/sites/11/2013/12/Ch-11-Dependency-Diagram-300x67.jpg[image:Database%20Design%20-%202nd%20Edition_fitxers/Ch-11-Dependency-Diagram-300x67.jpg[Ch-11-Dependency-Diagram-300x67]]

Figure 11.6. Dependency diagram.

The following dependencies are identified in this table:

* ProjectNo and EmpNo, combined, are the PK.
* Partial Dependencies:
** ProjectNo —> ProjName
** EmpNo —> EmpName, DeptNo, +
** ProjectNo, EmpNo —> HrsWork
* Transitive Dependency:
** DeptNo —> DeptName

=== Termes claus

*Armstrong’s axioms*:* *a set of inference rules used to infer all the
functional dependencies on a relational database

*DBA*: database administrator

*decomposition*: a rule that suggests if you have a table that appears
to contain two entities that are determined by the same PK, consider
breaking them up into two tables

*dependent*: the right side of the functional dependency diagram

*determinant*: the left side of the functional dependency diagram

*functional dependency (FD):* a relationship between two attributes,
typically between the PK and other non-key attributes within a table

*non-normalized table*: a table that has data redundancy in it

*Union*: a rule that suggests that if two tables are separate, and the
PK is the same, consider putting them together

== Normalització

=== Introducció

La normalització hauria d'ésser part del procés de disseny de la base de dades.
Tanmateix, és difícil separar el procés de normalització del procés de modelat
ER i per tant les dues tècniques s'haurien d'utilitzant concurrentment.

Utilitzem un diagrama entitat relació (ERD) per proporcionar un visió general,
o visió macro, dels requeriments i operacions de les dades d'una organització.
Aquest es crea a través d'un procés iteratiu que involucra la identificació
d'entitats rellevants, els seus atributs i les seves relacions.

El procés de normalització es focalitza en característiques d'entitats
específiques i representa la visió micro d'entitats dins del ERD.

=== Què és la normalització?

La _normalització_ és la branca de la teoria relacional que proporciona un
coneixement profund del disseny. És el procés de determinar quanta redundància
existeix en una taula. Els objectius de la normalització són:

* Poder caracteritzar el nivell de redundància en un esquema relacional.
* Proporcionar mecanismes per transformar esquemes per tal d'eliminar-ne
la redundància.

La teoria de normalització deriva fortament de la teoria de dependències
funcionals. La teoria de normalització defineix sis formes normals (NF). Cada
forma normal involucra un conjunt de propietats de dependència que un esquema
ha de complir i cada forma norma dóna garanties sobre la presència i/o
absència d'anomalies d'actualització. Això significa que les formes normals
més altes tenen menys redundància, i com a resultat, menys problemes
d'actualització.

=== Formes normals

Totes les taules de qualsevol base de dades poden estar en una de les formes
normals que discutirem a continuació. Idealment només volem redundància mínima
per relacionar PK amb FK. Qualsevol altra cosa hauria de derivar-se d'altres
taules. Hi ha sis formes normals, però només mirarem les quatre primeres, que
són:

* Primera forma normal (1NF).
* Segona forma normal (2NF).
* Tercera forma normal (3NF).
* Forma normal de Boyce-Codd (BCNF).

La BCNF s'utilitza rarament.

=== Primera forma normal (1NF)

En la _primera forma normal_, només es permeten valors simples a la intersecció
de cada fila i columna; així, no hi ha grups que es repeteixin.

Per normalitzar una relació que conté un grup repetitiu, traurem el grup
repetit i formarem dues noves relacions.

La PK de la nova relació és una combinació de la PK de la relació original
més un atribut de la relació nova que hem creat, per tal d'aconseguir la
identificació unívoca.

==== Procés per a la 1NF

Utilitzarem la taula *Student_Grade_Report* de sota, de la base de dades d'una
escola, com el nostre exemple per explicar el procés per assolir la 1NF.

*Student_Grade_Report*(StudentNo, StudentName, Major, CourseNo,
CourseName, InstructorNo, InstructorName, InstructorLocation, Grade)

* A la taula Student Grade Report, el grup que es repeteix és la informació
del curs. Un estudiant pot participar en molts cursos.
* Treure el grup repetitiu. En aquest cas, és la informació del curs per a cada
estudiant.
* Identificar la PK per a la nova taula.
* La PK ha d'identificar de forma única el valor de l'atribut (StudentNo i
CourseNo).
* Després de treure tots els atributs relacions al curs i a l'estudiant,
ens queda la taula student course (*StudentCourse*).
* La taula *Student* es troba ara en la primera forma normal amb el grup
repetitiu extret.
* A sota es mostren les dues noves taules:

*Student* (StudentNo, StudentName, Major)

*StudentCourse* (StudentNo, CourseNo, CourseName, InstructorNo,
InstructorName, InstructorLocation, Grade)

==== Com actualitzar les anomalies de la 1NF

*StudentCourse* (StudentNo, CourseNo, CourseName, InstructorNo,
InstructorName, InstructorLocation, Grade)

* Per afegir un nou curs, necessitem un estudiant.
* Quan s'ha d'actualitzar la informació del curs, podem obtenir inconsistències.
* Per esborrar un estudiant, també podríem eliminar informació crítica sobre
un curs.

=== Segona forma normal (2NF)

Per a la _segona forma normal_, la relació ha d'estar primer en la 1NF. La
relació es troba automàticament en 2NF si, i només si, la PK inclou un sol
atribut.

Si la relació té una PK composta, aleshores cada atribut que no sigui clau ha
de ser completament depenent de la totalitat de la PK i no d'un subconjunt de
la PK (per exemple, no hi ha d'haver una dependència parcial o augment).

==== Procés per a la 2NF

Per moure's a la 2NF, una taula ha d'estar primer en 1NF.

* La taula Student ja es troba en 2NF perquè té una PK d'una sola columna.
* Quan examinem la taula Student Course, veiem que no tots els atributs són
completament depenents de la PK; específicament, tota la informació del curs.
L'únic atribut que en depèn completament és la nota (grade).
* Identifiquem la nova taula que conté la informació del curs.
* Identifiquem la PK per a la nova taula.
* Les tres noves taules es mostren a sota:

*Student* (StudentNo, StudentName, Major)

*CourseGrade* (StudentNo, CourseNo, Grade)

*CourseInstructor* (CourseNo, CourseName, InstructorNo, InstructorName,
InstructorLocation)

==== Com actualitzar les anomalies 2NF

* Quan afegim un nou instructor, necessitem un curs.
* Actualitzar la informació d'un curs podria portar a inconsistència per a la
informació de l'instructor.
* Quan s'esborra un curs, també pot esborrar-se la informació de l'instructor.

=== Tercera forma normal (3NF)

Per estar en la _tercera forma normal_, la relació ha d'estar en la segona
forma normal. A més, totes les dependències transitives s'han d'eliminar; un
atribut no clau no pot ésser depenent funcionalment d'un altre atribut no clau.

==== Procés per a la 3NF

* Eliminem tots els atributs depenent en relacions transitives des de totes les
taules que tenen una relació transitiva.
* Creem noves taules amb les dependències extretes.
* Comprovem les noves taules així com les taules modificades per assegurar-nos
que cada taula té un determinant i que cap taule conté dependències
inapropiades.
* Mirem les següent quatre taules:

*Student* (StudentNo, StudentName, Major)

*CourseGrade* (StudentNo, CourseNo, Grade)

*Course* (CourseNo, CourseName, InstructorNo)

*Instructor* (InstructorNo, InstructorName, InstructorLocation)

En aquest estadi, no hi haurien d'haver anomalies en la tercera forma normal.
Anem a mirar el diagrama de dependències per aquest exemple, a continuació.
El primer pas és eliminar els grups repetits, com s'ha explicat més amunt.

*Student* (StudentNo, StudentName, Major)

*StudentCourse* (StudentNo, CourseNo, CourseName, InstructorNo,
InstructorName, InstructorLocation, Grade)

Per recordar el procés de normalització per a la base de dades School, revisa
les dependències mostrades a la figura:

.Diagrama de dependència.
image::images/dependency_diagram_school.jpg[Diagrama de dependència]

Les abreviatures utilitzades a la figura són les següents:

* PD: dependència parcial
* TD: dependència transitiva
* FD: dependència completa (nota: habitualment utilitzem FD per dependència
*funcional*. L'ús de FD com abreviatura per dependència completa només es fa
en aquesta figura).

=== Forma normal de Boyce-Codd (BCNF)

Quan una taula té més d'una clau candidata, poden sorgir anomalies fins i tot
quan la relació està en 3NF. La _forma normal de Boyce-Codd_ és un cas especial
de la 3NF. Una relació es troba en BCNF si, i només si, tots els determinants
són claus candidates.

==== Exemple 1 de BCNF

Considerem la taula següent (*St_Maj_Adv*).

[width="100%",cols="<34%,<33%,<33%",]
|===========
|*Student_id*|*Major*|*Advisor*
|111|Physics|Smith
|111|Music|Chan
|320|Math|Dobbs
|671|Physics|White
|803|Physics|Smith
|===========

Les _regles semàntiques_ (regles de negoci aplicades a la base de dades) per
aquesta taula són:

1. Cada estudiant pot especialitzar-se (Major) en diverses matèries.
2. Per cada especialitat, un estudiant donat té només un tutor (Advisor).
3. Cada especialitat té diversos tutors.
4. Cada tutor tutoritza només una especialitat.
5. Cada tutor tutoritza diversos estudiants d'una especialitat.

A continuació es mostren les dependències funcionals per aquesta taula. La
primera és una clau candidata; la segona no ho és.

1.  Student_id, Major ——>  Advisor
2.  Advisor  ——>  Major

Entre les anomalies per aquesta taula hi ha:

1. Eliminació - estudiant esborrar informació de tutor.
2. Inserció - un nou tutor necessita un estudiant.
3. Actualització - inconsistències.

*Nota*: cap atribut simple és una clau candidata.

La PK pot ésser Student_id, Major o Student_id, Advisor.

Per reduir la relació *St_Maj_Adv* a BCNF, crearem dues noves taules:

1.  *St_Adv* (Student_id, Advisor)
2.  *Adv_Maj* (Advisor, Major)

Taula *St_Adv*:

[width="100%",cols="<50%,<50%",]
|===========
|*Student_id*|*Advisor*
|111|Smith
|111|Chan
|320|Dobbs
|671|White
|803|Smith
|===========

Taula *Adv_Maj*:

[width="100%",cols="<50%,<50%",]
|========
|*Advisor*|*Major*
|Smith|Physics
|Chan|Music
|Dobbs|Math
|White|Physics
|========

==== Exemple 2 de BCNF

Considerem la següent taula (*Client_Interview)*.

[width="100%",cols="<20%,<20%,<20%,<20%,<20%",]
|==============
|*ClientNo*|*InterviewDate*|*InterviewTime*|*StaffNo*|*RoomNo*
|CR76|13-May-02|10.30|SG5|G101
|CR56|13-May-02|12.00|SG5|G101
|CR74|13-May-02|12.00|SG37|G102
|CR56|1-July-02|10.30|SG5|G102
|==============

FD1 – ClientNo, InterviewDate –> InterviewTime, StaffNo, RoomNo  (PK)

FD2 – staffNo, interviewDate, interviewTime –> clientNO (clau candidata: CK)

FD3 – roomNo, interviewDate, interviewTime –> staffNo, clientNo    (CK)

FD4 – staffNo, interviewDate –> roomNo

Una relació es troba en BCNF si, i només si, cada determinant és una clau
candidata. Hem de crear una taula que incorpori les primeres tres FD
(taula *Client_Interview2*) i una altra taula (taula *StaffRoom*) per la
quarta FD.

Taula *Client_Interview2*:

[width="100%",cols="<25%,<25%,<25%,<25%",]
|==============
|*ClientNo*|*InterviewDate*|*InterViewTime*|*StaffNo*
|CR76|13-May-02|10.30|SG5
|CR56|13-May-02|12.00|SG5
|CR74|13-May-02|12.00|SG37
|CR56|1-July-02|10.30|SG5
|==============

Taula *StaffRoom*:

[width="100%",cols="<34%,<33%,<33%",]
|==============
|*StaffNo*|*InterviewDate*|*RoomNo*
|SG5|13-May-02|G101
|SG37|13-May-02|G102
|SG5|1-July-02|G102
|==============

=== Normalització i disseny de bases de dades

Durant el procés de normalització del disseny d'una base de dades, cal
assegurar-se que les entitats proposades compleixen la forma normal
requerida abans que es de crear les estructures de les taules. Moltes
bases de dades del món real s'han dissenyat inapropiadament o carregat amb
anomalies si s'han modificat inapropiadament al llarg del temps. Et podrien
demanar que redissenyis o modifiques una base de dades existent. Aquest pot
ésser una gran tasca si les taules no estan correctament normalitzades.

=== Termes claus i abreviacions

*forma normal de Boyce-Codd (BCNF)*: una cas especial de tercera forma normal.

*primera forma normal (1NF)*: només es permeten valors simples a la intersacció
de cada fila i columna de manera que no hi hagi groups repetits.

*normalització*: el procés de determinar quanta redundància existeix en una
taula.

*segona forma normal (2NF)*: la relació ha d'estar en 1NF i la PK comprendre
un sol atribut.

*regles semàntiques*: regles de negoci aplicades a la base de dades.

*tercera forma normal (3NF)*: la relació ha d'estar en 2NF i totes les
dependències transitives han de ser eliminades; un atribut no clau no pot
ésser depenent funcionalment d'un altre atribut no clau.
